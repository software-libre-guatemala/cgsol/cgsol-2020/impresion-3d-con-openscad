altura_rosca=10;
diametro_rosca=15;
grosor=1;
diametro_tubo=10; //diámetro interno

cuerda=120;
$fn=cuerda;

module tapa_2d(r_a=altura_rosca,
               r_d=diametro_rosca,
               t_d=diametro_tubo,
               g=grosor){
	r_r=r_d/2;
	t_r=t_d/2;

	polygon(
		points=[
			[r_r, 0],[r_r+g,0],[r_r+g,r_a],[t_r,r_a],
			[t_r,r_a+t_r],[t_r-g,r_a+t_r],[t_r-g,r_a-g],
			[r_r,r_a-g],
			[r_r,0]
			]
		);
}

rotate_extrude(){
	minkowski(){
		tapa_2d();
		circle(d=1);
	}
}
