esfera_d = 100; // diametro 10cm
cuerda = 60; // cuerdas

$fn = cuerda;

module cilindro(diametro=esfera_d/2, altura=esfera_d){
	a = altura+0.1;
	cylinder(d=diametro, h=a, center=true);
}

difference(){
sphere(d=esfera_d, center=true);

rotate([0,0,0])
	cilindro();
rotate([90,0,0])
	cilindro();
rotate([0,90,0])
	cilindro();
}
rotate([90,0,0])
color("red", alpha=0.5)
	cilindro(altura=esfera_d*1.1);
