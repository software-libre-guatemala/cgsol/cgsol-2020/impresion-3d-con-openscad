texto="CGSOL 2020";
texto_a=50; //Tamaño fuente
texto_l=405; //Tamaño calculado de largo
texto_g=10; //grosor
margen=0.2*texto_a;

cuerdas=60;

$fn=cuerdas;

difference(){

translate([-margen,-margen,-texto_g/2])
	cube([2*margen+texto_l, 2*margen+texto_a, texto_g]);
linear_extrude(height=texto_g)
	text(text=texto, size=texto_a);
}
